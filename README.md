# Squeezebox-Web

This is a web based player for [Logitech Media Server](https://mysqueezebox.com/download)

If you bought the [hardware players](<https://en.wikipedia.org/wiki/Squeezebox_(network_music_player)>) about 10 years ago you've been enjoying them ever since.

Or maybe you've built a multi-room audio system out of open source [software](https://www.picoreplayer.org/) and cheap hardware.

Either way you use a centralised music server to stream music to different rooms (different streams or in sync)

But there hasn't been a web based player for this collection.

Squeezebox-web is that player

It doesn't sync with the other players (I may try again but I found browser caching caused problems).

But it does allow you to listen via a mobile or regular browser without installing specific software.

**This app is incomplete - and is no more than a proof of concept at the moment.**

## Features

- Lists Albums
- Plays albums
- Works on mobile or Desktop
- Integrates with media controls  
  (This means hardware buttons for pause/skip work and controls are shown on lock screen)
- Shows album cover art
- Installable as PWA if served via https

## Bugs / Limits

- May list albums that are not playable  
  (such as spotify)
- Can't browse any other way than by albums
- Doesn't play radio streams

## Future aims

- Offline playback
- Control other players
- Better browse options
- Personalisation  
  (different users different favourites)

## How to use it

I don't know how to make a custom skin to integrate with the LMS server so this runs as a standalone website

If you're not comfortable with some aspects of software development and debugging - don't use this yet.

### Development version

```bash
git clone https://gitlab.com/tangiblebytes/squeezebox/squeezebox-web.git
```

edit package.json and replace the proxy value with your LMS address

```bash
yarn install
yarn start
```

his will start a local version of the app and edits will be hotloaded.

The create a 'production' build

```bash
yarn build
```

This will generate static/minified files in the build directory.

### Docker

The easiest way to try it out is using docker

Change the LMS_HOST to your address

and run the latest docker image (hosted on gitlab)

```bash
docker run --env LMS_HOST="http://192.168.1.118:9000"  -dit --name squeezy  -p 8080:80 registry.gitlab.com/tangiblebytes/squeezebox/squeezebox-web
```

### Webserver

The app is just a set of static files but expects to find json, images and cover art on the same server paths

Apache config to do that is

```apache
ProxyPass "/jsonrpc.js"  "${LMS_HOST}/jsonrpc.js"
ProxyPassReverse "/jsonrpc.js"  "${LMS_HOST}/jsonrpc.js"

ProxyPass "/stream.mp3"  "${LMS_HOST}/stream.mp3"
ProxyPassReverse "/stream.mp3"  "${LMS_HOST}/stream.mp3"

ProxyPass "/music/"  "${LMS_HOST}/music/"
ProxyPassReverse "/music/"  "${LMS_HOST}/music/"
```

The site will be in the `build` directory after running `yarn build`

Or can be [downloaded from gitlab](https://gitlab.com/tangiblebytes/squeezebox/squeezebox-web/-/jobs/artifacts/master/download?job=build_react)

The code includes a manifest.json file which allows the site to be installed as a [PWA](https://web.dev/progressive-web-apps/) but only if servered from a https site with a valid cert

You'd also need to edit the manifest to include your URL.

Installing as a PWA means the site appears more like a regular app on mobile with an icon on the homescreen and no address bar displayed in use. It also caches data and can be used offline - though code to cache the actual music has not yet been written - as a proof of concept though you can browse album listings offline once toy have populated caches.
