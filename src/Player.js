import React from "react";
import ReactJkMusicPlayer from "react-jinke-music-player";
import "react-jinke-music-player/assets/index.css";

function Player(props) {
  // Note: the empty deps array [] means
  // this useEffect will run once
  // similar to componentDidMount()

  return (
    (props.visible && (
      <ReactJkMusicPlayer
        audioLists={[
          {
            musicSrc: "http://fruitytunes:9000/music/10147/download.mp3",
            name: "Some music ",
          },
          {
            musicSrc: "http://fruitytunes:9000/music/11820/download.mp3",
            name: "Some More music ",
          },
        ]}
        autoPlay={false}
        mode="full"
      />
    )) || (
      <figure>
        <figcaption>Free the Music</figcaption>
      </figure>
    )
  );
}

export default Player;
