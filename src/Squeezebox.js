import React, { useState, useEffect } from "react";
import AlbumList from "./AlbumList";
import Album from "./Album";
// import Player from "./Player";

function Squeezebox() {
  const [album, setAlbum] = useState({ id: 0 });
  const [status, setStatus] = useState({ "info total albums": 0 });
  useEffect(() => {
    fetch("/jsonrpc.js?page=serverstatus", {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",

      body: '{"method":"slim.request","params":["",["serverstatus"]]}',
    })
      .then((res) => res.json())
      .then(
        (result) => {
          setStatus(result.result);
        },
        (error) => {
          //setError(error);
          console.log(error);
        }
      );
  }, []);

  // const [playerVisible, setPlayerVisible] = useState(true);
  return (
    <div>
      {/* <Album albumID={albumID} setPlayerVisible={setPlayerVisible} /> */}

      {album.id !== 0 && <Album album={album} />}
      {/* <Player visible={playerVisible} /> */}
      <AlbumList setAlbum={setAlbum} status={status} />
    </div>
  );
}

export default Squeezebox;
