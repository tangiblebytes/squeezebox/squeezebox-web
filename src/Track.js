import React, { useState, useEffect } from "react";

function Track() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);

  // Note: the empty deps array [] means
  // this useEffect will run once
  // similar to componentDidMount()
  useEffect(() => {
    fetch("/jsonrpc.js", {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",

      // get tracks for album
      body:
        // item_id seems to select less data
        '{"id":7,"method":"slim.request","params":["00:04:20:2a:98:29",["trackinfo","items",0,25000,"track_id:10625","item_id:9","isContextMenu:1","menu:trackinfo"]]}',

      //'{"id":7,"method":"slim.request","params":["00:04:20:2a:98:29",["trackinfo","items",0,25000,"track_id:10625","isContextMenu:1","menu:trackinfo"]]}',
    })
      .then((res) => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          console.log(result);
          let albums = result.result.item_loop;

          setItems(albums);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      );
  }, []);

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    // http://fruitytunes:9000/music/9266/download.mp3
    // http://fruitytunes:9000/stream.mp3?player=XX:XX:XX:XX:XX:XX
    return <pre>{JSON.stringify(items, null, 2)}</pre>;
  }
}
export default Track;
