import React, { useState, useEffect, useRef } from "react";
import ReactJkMusicPlayer from "react-jinke-music-player";
import "react-jinke-music-player/assets/index.css";
import "./player.css";

function Album(props) {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [audioList, setaudioList] = useState([]);

  const [Autoplay, setAutoPlay] = useState(false);
  const album = props.album;
  const player = useRef(null);
  // const play = (track_id) => {
  //   //["playlistcontrol", "cmd:"+<cmd>, "artist_id:"+<artist_id>]
  //   var body =
  //     '{"method":"slim.request","params":["te:st:in:gX",["playlistcontrol", "cmd:load", "track_id:' +
  //     track_id +
  //     '"]]}';
  //   fetch("/jsonrpc.js?" + body, {
  //     headers: {
  //       "Content-Type": "application/json",
  //     },
  //     method: "POST",

  //     // get tracks for album
  //     body: body,
  //   })
  //     .then((res) => res.json())
  //     .then(
  //       (result) => {
  //         console.log("playing");
  //         props.setPlayerVisible(true);
  //       },
  //       (error) => {
  //         console.log("error");
  //       }
  //     );
  // };
  // Note: the empty deps array [] means
  // this useEffect will run once
  // similar to componentDidMount()
  useEffect(() => {
    if (album.id === 0) {
      return;
    }
    // console.log(album);
    var body =
      '{"method":"slim.request","params":["",["tracks", 0, 10, "album_id:' +
      album.id +
      '"]]}';
    if (album === {}) {
      return;
    }
    fetch("/jsonrpc.js?" + body, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",

      // get tracks for album
      body: body,
    })
      .then((res) => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setAutoPlay(true);
          //  console.log(result);
          let albums = result.result.titles_loop;

          let audioList = albums.map((item) => {
            return {
              musicSrc: "/music/" + item.id + "/download.mp3",
              name: item.title,
              singer: item.artist,
              cover: "music/" + album.artwork_track_id + "/cover_300x300_f",
            };
          });
          setaudioList(audioList);
          // when starting a new album -= make sure the player is visible
          // https://github.com/lijinke666/react-music-player/blob/master/src/index.js#L103
          player.current.setState({ toggle: true });
          //player.setToggle(true);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      );
  }, [album, player]);

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Choose an Album to Play</div>;
  } else {
    return (
      <div>
        <ReactJkMusicPlayer
          ref={player}
          audioLists={audioList}
          autoPlay={Autoplay}
          mode="full"
          theme="light"
          showThemeSwitch={false}
          preload={true}
          showMediaSession
          clearPriorAudioLists
        />
        <h1>
          {album.artist} - {album.album}
        </h1>
        {/* {JSON.stringify(album)} */}
      </div>
    );
  }
}
export default Album;
