import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import Pagination from "react-bootstrap/Pagination";

function AlbumList(props) {
  const setAlbum = props.setAlbum;
  const albumCount = props.status["info total albums"];
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const albumsPerPage = 10;
  const [pageNumber, setPageNumber] = useState(1);

  let pages = [];
  let firstPage = pageNumber <= 4 ? 1 : pageNumber - 4;
  let lastPossiblePage = albumCount / albumsPerPage;
  let lastPage =
    pageNumber + 4 > lastPossiblePage ? lastPossiblePage + 1 : pageNumber + 4;
  for (let page = firstPage; page <= lastPage; page++) {
    pages.push(
      <Pagination.Item
        onClick={() => setPageNumber(page)}
        key={page}
        active={page === pageNumber}
      >
        {page}
      </Pagination.Item>
    );
  }

  const paginationBasic = (
    <div>
      <Pagination>{pages}</Pagination>
    </div>
  );

  // Note: the empty deps array [] means
  // this useEffect will run once
  // similar to componentDidMount()
  useEffect(() => {
    const start = (pageNumber - 1) * albumsPerPage;
    fetch(
      "/jsonrpc.js?page=albums&start=" +
        start +
        "&itemsPerResponse=" +
        albumsPerPage,
      {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        // {'method': 'slim.request', 'params': [<player_mac_address>, <command>]}
        // where <player_mac_address> is the squeezebox's MAC address that you are controlling (or "-" to get server info).
        // and <command> is the command you want to send.

        //status
        //body: '{"id":1,"method":"slim.request","params":["",["serverstatus"]]}',
        //    musicfolder <start> <itemsPerResponse> <taggedParameters></taggedParameters>
        // get all albums
        body:
          '{"method":"slim.request","params":["",["albums", ' +
          start +
          ", " +
          albumsPerPage +
          ', "tags:jal"]]}',
      }
    )
      .then((res) => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          let albums = result.result.albums_loop;

          setItems(albums);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      );
  }, [pageNumber]);

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading ...</div>;
  } else {
    return (
      <div>
        {paginationBasic}
        <ul>
          {items.map((item) => (
            <li key={item.id}>
              <p>
                <Button onClick={(e) => setAlbum(item)}>
                  {item.album} - {item.artist}
                </Button>
              </p>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
export default AlbumList;
